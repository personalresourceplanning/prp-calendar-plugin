/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class CalendarController {

    constructor(translationLoader, calendarService) {
        this.service = calendarService;
        this.trans = translationLoader;
        this.logger = new Logger("CalendarController");
    }

    async createEvent(event) {
        event.preventDefault();
        let startDate = document.getElementById("eventStartDate").value;
        let endDate = document.getElementById("eventEndDate").value;

        if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
            document.getElementById("eventEndDate").classList.add("errorInput");
            document.getElementById("errorTextEventEndDate").hidden = false;
        } else {
            $('#createEventModal').modal('hide');

            let eventTitle = document.getElementById("eventTitle").value;
            if (eventTitle == "") {
                eventTitle = this.trans.calendar.noTitle;
            }

            //read dates
            let startDate = document.getElementById("eventStartDate").value;
            let endDate = document.getElementById("eventEndDate").value;
            //format dates for allDay
            if (document.getElementById("allDayCheckbox").checked) {
                if (startDate !== "") {
                    //create date object
                    startDate = new Date(startDate);
                } else {
                    //create date object with current time
                    startDate = new Date();
                }
                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate = moment(startDate).format('DD-MM-YYYY HH:mm');

                if (endDate !== "") {
                    endDate = new Date(endDate);
                } else {
                    endDate = new Date();
                }
                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate = moment(endDate).format('DD-MM-YYYY HH:mm');

            } else {
                //format dates with time
                if (startDate !== "") {
                    startDate = new Date(startDate);
                } else {
                    startDate = new Date();
                }
                startDate = moment(startDate).format('DD-MM-YYYY HH:mm');

                if (endDate !== "") {
                    endDate = new Date(endDate);
                } else {
                    endDate = new Date();
                }
                endDate = moment(endDate).format('DD-MM-YYYY HH:mm');
            }

            let event = {
                "title": eventTitle,
                "description": document.getElementById("eventDescription").value,
                "endDate": endDate,
                "startDate": startDate,
                "location": document.getElementById("eventLocation").value
            }

            let calendarId = window.location.href.split('#')[1].split('?')[1].split('&')[0].substr(11);

            //call server
            await this.service.createEventInCalendar(calendarId, event);

            //reset Form
            createEventForm.reset();

            //reset error message if necessary
            if (document.getElementById("errorTextEventEndDate").hidden == false) {
                document.getElementById("errorTextEventEndDate").hidden = true;
                document.getElementById("eventEndDate").classList.remove("errorInput");
            }

            //reset allDay Checkbox if necessary
            if (document.getElementById("allDayCheckbox").checked) {
                document.getElementById("eventStartDate").type = "datetime-local";
                document.getElementById("eventEndDate").type = "datetime-local";
            }
            window.location.reload();
        }
    }

    async createCalendar() {
        let calendarTitle = document.getElementById("calendarTitle").value;
        if (calendarTitle == "") {
            document.getElementById("calendarTitle").classList.add("errorInput");
            document.getElementById("errorTextCalendarTitle").hidden = false;
        } else {
            $('#createCalendarModal').modal('hide');

            let calendar = {
                "name": calendarTitle,
                "description": document.getElementById("calendarDescription").value
            }
            this.logger.log(calendar)
            //call Server
            await this.service.createNewCalendar(calendar);

            //reset form
            document.createCalendarForm.reset();

            //reset error message if necessary
            if (document.getElementById("errorTextCalendarTitle").hidden == false) {
                document.getElementById("errorTextCalendarTitle").hidden = true;
                document.getElementById("calendarTitle").classList.remove("errorInput");
            }
            window.location.reload();
        }
    }

    async editCalendarEvent(event) {
        event.preventDefault();
        //check if startDate is correct
        let startDate = new Date(document.getElementById("eventDetailStartDate").value);
        let endDate = new Date(document.getElementById("eventDetailEndDate").value);
        if (startDate.getTime() > endDate.getTime()) {
            document.getElementById("eventDetailEndDate").classList.add("errorInput");
            document.getElementById("eventDetailErrorTextEndDate").hidden = false;
        } else {
            let calendarEvent = {};

            calendarEvent.id = parseInt(document.getElementById('eventDetailTitleEdit').dataset.eventid);
            calendarEvent.calendarId = parseInt(document.getElementById('eventDetailTitleEdit').dataset.calendarid);
            calendarEvent.title = document.getElementById('eventDetailTitleEdit').value;
            calendarEvent.location = document.getElementById('eventDetailLocation').value;
            calendarEvent.description = document.getElementById('eventDetailDescription').value;

            let startDate = new Date(document.getElementById('eventDetailStartDate').value);
            let endDate = new Date(document.getElementById('eventDetailEndDate').value);
            calendarEvent.startDate = moment(startDate).format('DD-MM-YYYY HH:mm');
            calendarEvent.endDate = moment(endDate).format('DD-MM-YYYY HH:mm');

            this.logger.log(JSON.stringify(calendarEvent))

            await this.service.updateExistingEvent(calendarEvent);

            window.location.href = "#/calendar/dailyview?calendarId=" + calendarEvent.calendarId;
        }
    }

    async deleteCalendarEvent(event) {
        event.preventDefault();
        let calendarEvent = {};
        calendarEvent.id = parseInt(document.getElementById('eventDetailTitleEdit').dataset.eventid);
        calendarEvent.calendarId = parseInt(document.getElementById('eventDetailTitleEdit').dataset.calendarid);

        await this.service.deleteExistingEvent(calendarEvent);

        window.location.href = "#/calendar/dailyview?calendarId=" + calendarEvent.calendarId;
    }

    async getCalendarList() {
        let calendarList = [];
        await this.service.loadAllCalendars().then(data => {
            calendarList = data;
        }).catch((error) => {
            this.logger.warn(JSON.stringify(error));
        });
        return calendarList;
    }

    async getEventsFromCalendar(calendarId, date) {
        let events = [];
        await this.service.loadAllEventsForDate(date).then(data => {
            data.forEach((event) => {
                if (event.calendarId === parseInt(calendarId, 10)) {
                    events.push(event);
                }
            });
        }).catch((error) => {
            this.logger.warn(JSON.stringify(error));
        });
        return events;
    }

    async getEvent(eventId, date) {
        let event;
        await this.service.loadAllEventsForDate(date).then(data => {
            data.forEach(eventFromRequest => {
                if (eventFromRequest.id === eventId) {
                    event = eventFromRequest;
                }
            });
        }).catch(error => {
            this.logger.warn(JSON.stringify(error));
        });
        return event;
    }

}
