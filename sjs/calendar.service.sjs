/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class CalendarService {

    constructor() {
        this.token = "Bearer " + LOCALSTOREMANAGER.loadAccessToken();
        this.domain = "http://prp.error-cloud.com:8080";
        this.logger = new Logger("CalendarService");
    }

    /******************************
     Calendar
     *****************************/

    async loadAllCalendars() {
        let ref = this;
        ref.logger.log("Loading all calendars");

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar",
                    method: "GET",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        ref.logger.log("Load all calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (error) {
                        ref.logger.warn("Getting all calendars request failed: jqXHR: " + JSON.stringify(error));
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async createNewCalendar(calendar) {
        let ref = this;

        ref.logger.log("Creating new calendar" + JSON.stringify(calendar));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/create",
                    method: "POST",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(calendar),
                    success: function (data) {
                        ref.logger.log("Create calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Create calendar request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async loadCalendarById(calendarId) {
        let ref = this;

        ref.logger.log("Loading calendar: " + JSON.stringify(calendarId));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId,
                    method: "GET",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        ref.logger.log("Load calendar by Id request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Load calendar by Id request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async updateCalendarById(calendar) {
        let ref = this;

        ref.logger.log("Deleting calendar: " + JSON.stringify(calendar));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendar.id,
                    method: "PUT",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(calendar),
                    success: function (data) {
                        ref.logger.log("Update calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Update calendar request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async deleteCalendarById(calendarId) {
        let ref = this;

        ref.logger.log("Deleting calendar: " + calendarId);

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId,
                    method: "DELETE",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        ref.logger.log("Delete calendar request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Delete calendar request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    /******************************
     Events
     *****************************/

    async loadAllEvents() {
        let ref = this;

        ref.logger.log("Loading all events");

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/event/list",
                    method: "GET",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        ref.logger.log("Load all Events request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Load all Events request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });


    }

    async loadAllEventsForDate(date) {
        let ref = this;

        ref.logger.log("Loading all events");

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/event/list?date=" + encodeURI(date),
                    method: "GET",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        ref.logger.log("Load all Events for Date request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Load all Events for Date request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async createEventInCalendar(calendarId, event) {
        let ref = this;

        ref.logger.log("Creating event for calendar: " + calendarId + " " + JSON.stringify(event));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event",
                    method: "POST",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(event),
                    success: function (data) {
                        ref.logger.log("Create event request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Create event request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async updateExistingEvent(calendarEvent) {
        let ref = this;

        ref.logger.log("Updating event for calendar: " + calendarEvent.calendarId + " " + JSON.stringify(calendarEvent));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarEvent.calendarId + "/event/" + calendarEvent.id,
                    method: "PUT",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(calendarEvent),
                    success: function (data) {
                        ref.logger.log("Update event request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Update event request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async deleteExistingEvent(calendarEvent) {
        let ref = this;

        ref.logger.log("Deleting event for calendar: " + calendarEvent.calendarId + " " + JSON.stringify(calendarEvent));

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarEvent.calendarId + "/event/" + calendarEvent.id,
                    method: "DELETE",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        ref.logger.log("Delete event request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Delete event request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    /******************************
     Reminder
     ******************************/

    async createReminderForEvent(calendarId, eventId, date) {
        let ref = this;

        ref.logger.log("Creating reminder for calendar: " + calendarId + " EventID: " + eventId + " Date: " + date);

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event/" + eventId + "/reminder",
                    method: "POST",
                    headers: {
                        "Authorization": this.token
                    },
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(date),
                    success: function (data) {
                        ref.logger.log("Create event reminder request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Create event reminder request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

    async deleteReminderForEvent(calendarId, eventId, date) {
        let ref = this;

        ref.logger.log("Deleting reminder for calendar: " + calendarId + " EventID: " + eventId + " Date: " + date);

        return new Promise(async (resolve, reject) => {
            try {
                await $.ajax({
                    url: this.domain + "/api/0.1/calendar/" + calendarId + "/event/" + eventId + "/reminder/" + date,
                    method: "DELETE",
                    headers: {
                        "Authorization": this.token
                    },
                    success: function (data) {
                        ref.logger.log("Delete event reminder request was successful. Data: " + JSON.stringify(data));
                        resolve(data);
                    },
                    error: function (data, textStatus, errorThrown) {
                        ref.logger.warn("Delete event reminder request failed: jqXHR: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }

}
