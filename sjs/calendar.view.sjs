/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class CalendarView {

    constructor(translationLoader) {
        this.service = new CalendarService();
        this.controller = new CalendarController(translationLoader, this.service);
        this.trans = translationLoader;
    }

    createCalendarTitle() {
        let calendarTitleTemplate = document.getElementById('calendarTitleTemplate').content;
        let calendarTitleTemplateCopy = document.importNode(calendarTitleTemplate, true);
        calendarTitleTemplateCopy.querySelector("#titleCalendarPlugin").textContent = this.trans.calendar.titleCalendarPlugin;
        document.getElementById("title-area").appendChild(calendarTitleTemplateCopy);
    }

    createSwitchButton(calendarId) {
        document.getElementById("switch-button-radio-one-label").textContent = this.trans.calendar.calendarView;
        document.getElementById("switch-button-radio-one").setAttribute("value", this.trans.calendar.calendarView);
        document.getElementById("switch-button-radio-one").addEventListener("click", function (e) {
            window.location.href = "#/calendar/dailyview?calendarId=" + calendarId;
        });

        document.getElementById("switch-button-radio-two-label").textContent = this.trans.calendar.eventView;
        document.getElementById("switch-button-radio-two").setAttribute("value", this.trans.calendar.eventView);
        document.getElementById("switch-button-radio-two").addEventListener("click", function (e) {
            window.location.href = "#/calendar/eventview?calendarId=" + calendarId;
        });
    }

    /******************************
     Calendar Modals
     *****************************/

    createCalendarPlusButton() {
        this.createCalendarModal();
        document.getElementById("createCalendarPlusButton").textContent = this.trans.calendar.addCalendar;
    }

    createEventPlusButton() {
        this.createEventModal();
        document.getElementById("createEventPlusButton").textContent = this.trans.calendar.addEvent;
    }

    //initilize createCalendarModal
    createCalendarModal() {
        let createCalendarModalTemplate = document.getElementById('modalTemplate').content;

        let createCalendarModalTemplateCopy = document.importNode(createCalendarModalTemplate, true);

        createCalendarModalTemplateCopy.querySelector('.modal').setAttribute('id', 'createCalendarModal');
        createCalendarModalTemplateCopy.querySelector('.modal-title').textContent = this.trans.calendar.addCalendar;
        createCalendarModalTemplateCopy.querySelector('.btn').textContent = this.trans.commons.save;

        createCalendarModalTemplateCopy.querySelector('.btn').addEventListener("click", this.controller.createCalendar.bind(this.controller));

        let createCalendarForm = document.getElementById('createCalendarFormTemplate').content;
        let copyCreateCalendarForm = document.importNode(createCalendarForm, true);

        copyCreateCalendarForm.querySelector('[for=calendarTitle]').textContent = this.trans.calendar.title + ":";
        copyCreateCalendarForm.querySelector('[for=calendarDescription]').textContent = this.trans.calendar.description + ":";
        copyCreateCalendarForm.querySelector('#errorTextCalendarTitle').textContent = this.trans.calendar.errorTextCalendarTitle;


        createCalendarModalTemplateCopy.querySelector('.modal-body').appendChild(copyCreateCalendarForm);
        document.getElementById("calendarContent").appendChild(createCalendarModalTemplateCopy);
    }

    //initilize createEventModal
    createEventModal() {
        let createEventModalTemplate = document.getElementById('modalTemplate').content;

        let createEventModalTemplateCopy = document.importNode(createEventModalTemplate, true);

        createEventModalTemplateCopy.querySelector('.modal').setAttribute('id', 'createEventModal');
        createEventModalTemplateCopy.querySelector('.modal-title').textContent = this.trans.calendar.addEvent;
        createEventModalTemplateCopy.querySelector('.btn').textContent = this.trans.commons.save;
        createEventModalTemplateCopy.querySelector('.btn').addEventListener("click", this.controller.createEvent.bind(this.controller));


        let createEventForm = document.getElementById('createEventFormTemplate').content;
        let copyCreateEventForm = document.importNode(createEventForm, true);

        //set texts
        copyCreateEventForm.querySelector('[for=eventTitle]').textContent = this.trans.calendar.title + ":";
        copyCreateEventForm.querySelector('[for=allDayCheckbox]').textContent = this.trans.calendar.allDay;
        copyCreateEventForm.querySelector('[for=eventLocation]').textContent = this.trans.calendar.location + ":";
        copyCreateEventForm.querySelector('[for=eventStartDate]').textContent = this.trans.calendar.startDate + ":";
        copyCreateEventForm.querySelector('[for=selectedEventDuration]').textContent = this.trans.calendar.duration + ":";
        copyCreateEventForm.querySelector('[for=eventEndDate]').textContent = this.trans.calendar.endDate + ":";
        copyCreateEventForm.querySelector('[for=eventDescription]').textContent = this.trans.calendar.description + ":";
        copyCreateEventForm.querySelector('#errorTextEventEndDate').textContent = this.trans.calendar.errorTextEventEndDate;

        createEventModalTemplateCopy.querySelector('.modal-body').appendChild(copyCreateEventForm);

        document.getElementById("calendarContent").appendChild(createEventModalTemplateCopy);

        document.getElementById("eventStartDate").min = moment().format('YYYY-MM-DDTHH:mm');

        this.createEventDurationSelect(document.getElementById("selectedEventDuration"));

        //add events
        document.getElementById("eventStartDate").addEventListener("change", this.setMinEndDate);
        document.getElementById("eventStartDate").addEventListener("change", this.calculateEndDate);
        document.getElementById("selectedEventDuration").addEventListener("change", this.calculateEndDate);
        document.getElementById("eventEndDate").addEventListener("change", this.setMaxStartDate);
        document.getElementById("eventEndDate").addEventListener("change", this.calculateStartDate);
        document.getElementById("allDayCheckbox").addEventListener("change", this.allDayCheckBoxChange);
    }

    calculateEndDate() {
        let startDate = document.getElementById("eventStartDate").value;
        let duration = document.getElementById("selectedEventDuration").value;
        if (startDate !== "" && duration !== "") {
            let calculatedEndDate = moment(startDate).add(duration);
            let formattedDate = moment(calculatedEndDate).format('YYYY-MM-DDTHH:mm:ss');
            document.getElementById("eventEndDate").value = formattedDate;
        }
    }

    calculateStartDate() {
        let endDate = document.getElementById("eventEndDate").value;
        let duration = document.getElementById("selectedEventDuration").value;
        if (endDate !== "" && duration !== "") {
            let calculatedStartDate = moment(endDate).subtract(duration);
            let formattedDate = moment(calculatedStartDate).format('YYYY-MM-DDTHH:mm:ss');
            document.getElementById("eventStartDate").value = formattedDate;
        }
    }

    allDayCheckBoxChange() {
        let checked = document.getElementById("allDayCheckbox").checked;
        if (checked) {
            document.getElementById("eventStartDate").type = "date";
            document.getElementById("eventEndDate").type = "date";
        } else {
            document.getElementById("eventStartDate").type = "datetime-local";
            document.getElementById("eventEndDate").type = "datetime-local";

        }
    }

    createEventDurationSelect(select) {

        let option = new Option("", "");
        select.add(option, undefined);

        let minutes = 15;
        while (minutes < 1440) {
            let duration = moment.utc(moment.duration(minutes, "minutes").asMilliseconds()).format("HH:mm");
            let option = new Option(duration, duration);
            select.add(option, undefined);
            minutes = minutes + 15;
        }

    }

    setMinEndDate() {
        document.getElementById("eventEndDate").min = document.getElementById("eventStartDate").value;
    }

    setMaxStartDate() {
        document.getElementById("eventStartDate").max = document.getElementById("eventEndDate").value;
    }

    /******************************
     Calendar calendarItems
     *****************************/

    async createCalendarItems() {
        let calendarList = [];
        calendarList = await this.controller.getCalendarList();
        if (calendarList.length > 0) {
            calendarList.forEach((calendar) => {
                let calendarItemTemplate = document.getElementById('calendarItemTemplate').content;
                let calendarItemTemplateCopy = document.importNode(calendarItemTemplate, true);

                calendarItemTemplateCopy.querySelector('.calendarItemAnchor').setAttribute('href', '#/calendar/dailyview?calendarId=' + calendar.id);
                calendarItemTemplateCopy.querySelector('.calendarItemTitle').textContent = calendar.name;
                calendarItemTemplateCopy.querySelector('.calendarItemDescription').textContent = calendar.description;

                document.getElementById('calendarList').appendChild(calendarItemTemplateCopy);
            });
        } else {
            // message to the user that no calendars have been found
            let calendarNotFoundMessageTemplate = document.getElementById('calendarsNotFound').content;
            let calendarNotFoundMessageTemplateCopy = document.importNode(calendarNotFoundMessageTemplate, true);
            calendarNotFoundMessageTemplateCopy.querySelector('#calendarNotFoundMessage').textContent = this.trans.calendar.noCalendarFound;
            document.getElementById('calendarList').appendChild(calendarNotFoundMessageTemplateCopy);
        }
    }

    /******************************
     Calendar Daily View
     *****************************/

    async createDailyView(calendarId, date) {

        if (calendarId === undefined || calendarId === "") {
            window.location.href = "#/calendar";
        }

        //check if date is set
        if (date === undefined || date === "") {
            date = moment().format('DD-MM-yyy HH:mm')
        }

        //create copy of template
        let dailyViewTemplate = document.getElementById('day-view-template').content;
        let dailyViewTemplateCopy = document.importNode(dailyViewTemplate, true);

        //set texts detailPageTitle
        dailyViewTemplateCopy.querySelector('#day').innerHTML = moment().format('dddd');
        dailyViewTemplateCopy.querySelector('#number').innerHTML = moment().format('LL');

        //append daily View of calendar
        document.getElementById('calendarContent').appendChild(dailyViewTemplateCopy);

        let events = await this.controller.getEventsFromCalendar(calendarId, date);
        events.forEach((event) => {
            this.createDailyViewEvent(event);
        })
    }

    createDailyViewEvent(event) {
        // check if other events are in the same timeframe and set the width accordingly
        let width = 50;
        let left = 0;
        let zIndex = 5;
        //get all the rendered events of that day
        let eventsArray = Array.from(document.querySelector(".events-container").children);

        if (eventsArray !== undefined || eventsArray !== []) {
            eventsArray.forEach(eventInCalendar => {
                //get the start and end dates of a rendered event
                let startDateInCalendar = moment(eventInCalendar.children[2].dataset.startdate, "DD-MM-yyy HH:mm");
                let endDateInCalendar = moment(eventInCalendar.children[2].dataset.enddate, "DD-MM-yyy HH:mm");

                //get the start date of the event to be rendered
                let eventStartDate = moment(event.startDate, "DD-MM-yyy HH:mm");
                let eventEndDate = moment(event.endDate, "DD-MM-yyy HH:mm");

                //check if these are in between each other and manipulate their width and left
                if (eventStartDate.isBetween(startDateInCalendar, endDateInCalendar) || eventEndDate.isBetween(startDateInCalendar, endDateInCalendar) || startDateInCalendar.isBetween(eventStartDate, eventEndDate) || endDateInCalendar.isBetween(eventStartDate, eventEndDate)) {
                    left = 100 - width;
                    width /= 2;
                    zIndex += 5;
                }
            });
        }
        //create copy of template
        let eventViewTemplate = document.getElementById('event-template').content;
        let eventViewTemplateCopy = document.importNode(eventViewTemplate, true);

        eventViewTemplateCopy = this.createEventTemplate(event, eventViewTemplateCopy);

        //calculate the height of the event
        let duration = this.calculateEventDuration(event.startDate, event.endDate);
        duration = (moment(duration, "HH:mm").hours() * 60) + moment(duration, "HH:mm").minutes();
        let rowPosition = this.calculateRowPosition(event.startDate);
        eventViewTemplateCopy.querySelector('.event-anchor').setAttribute('style', 'height: ' + duration + 'px; grid-row: ' + rowPosition + ';width: ' + width + '%; left: ' + left + '%; z-index:' + zIndex);

        document.querySelector('.events-container').appendChild(eventViewTemplateCopy);
    }

    createEventTemplate(event, eventTemplateCopy) {
        eventTemplateCopy.querySelector('.event-anchor').setAttribute('href', '#/calendar/event?eventId=' + event.id + "&date=" + encodeURI(event.startDate));
        eventTemplateCopy.querySelector('.event-name').textContent = event.title;
        eventTemplateCopy.querySelector('.event-location').textContent = event.location;
        eventTemplateCopy.querySelector('.event-description').textContent = event.description;

        let startDate = moment(event.startDate, "DD-MM-yyy HH:mm").format("DD.MM.yyy HH:mm");
        let endDate = moment(event.startDate, "DD-MM-yyy HH:mm").format("DD.MM.yyy HH:mm");

        eventTemplateCopy.querySelector('.event-date').setAttribute("data-startdate", event.startDate);
        eventTemplateCopy.querySelector('.event-date').setAttribute("data-enddate", event.endDate);
        eventTemplateCopy.querySelector('.event-date').textContent = startDate + " - " + endDate;
        return eventTemplateCopy;
    }

    calculateEventDuration(startDate, endDate) {
        startDate = moment(startDate, "DD-MM-yyy HH:mm");
        endDate = moment(endDate, "DD-MM-yyy HH:mm");
        let diff = endDate.diff(startDate);
        return moment.utc(diff).format("HH:mm");
    }

    calculateRowPosition(startDate) {
        startDate = moment(startDate, "DD-MM-yyy HH:mm");

        let h = moment(startDate, "HH:mm").hours();
        let m = moment(startDate, "HH:mm").minutes();

        let hour = h * 4;
        let minutes = Math.round(m / 15);

        let rowPos = hour + minutes;

        return rowPos + 1;
    }

    /******************************
     Calendar Event Detail Page
     *****************************/

    async createEventDetailPage(eventId, date) {
        // check if calendarId and date is set
        if (eventId === undefined || eventId === "" || date === undefined || date === "") {
            window.location.href = "#/calendar";
        }
        // get event data
        let event = await this.controller.getEvent(parseInt(eventId), date);
        // get Template
        let eventDetailPageTemplate = document.getElementById('eventDetailPageTemplate').content;
        let eventDetailPageTemplateCopy = document.importNode(eventDetailPageTemplate, true);

        // fill template with the text of all labels and buttons
        eventDetailPageTemplateCopy.querySelector('#eventDetailTitle').textContent = event.title;
        eventDetailPageTemplateCopy.querySelector('#eventDetailSaveButton').textContent = this.trans.commons.save;
        eventDetailPageTemplateCopy.querySelector('#eventDetailDeleteButton').textContent = this.trans.commons.delete;
        eventDetailPageTemplateCopy.querySelector('[for=eventDetailTitleEdit]').textContent = this.trans.calendar.title;
        eventDetailPageTemplateCopy.querySelector('[for=eventDetailLocation]').textContent = this.trans.calendar.location;
        eventDetailPageTemplateCopy.querySelector('[for=eventDetailDescription]').textContent = this.trans.calendar.description;
        eventDetailPageTemplateCopy.querySelector('[for=eventDetailStartDate]').textContent = this.trans.calendar.startDate;
        eventDetailPageTemplateCopy.querySelector('[for=eventDetailSelectedDuration]').textContent = this.trans.calendar.duration;
        eventDetailPageTemplateCopy.querySelector('[for=eventDetailEndDate]').textContent = this.trans.calendar.endDate;
        eventDetailPageTemplateCopy.querySelector('#eventDetailErrorTextEndDate').textContent = this.trans.calendar.errorTextEventEndDate;

        // fill template with information of the event
        eventDetailPageTemplateCopy.querySelector('#eventDetailTitleEdit').setAttribute('value', event.title);
        eventDetailPageTemplateCopy.querySelector('#eventDetailTitleEdit').setAttribute('data-eventId', event.id);
        eventDetailPageTemplateCopy.querySelector('#eventDetailTitleEdit').setAttribute('data-calendarId', event.calendarId);
        eventDetailPageTemplateCopy.querySelector('#eventDetailLocation').textContent = event.location;
        eventDetailPageTemplateCopy.querySelector('#eventDetailDescription').textContent = event.description;
        eventDetailPageTemplateCopy.querySelector('#eventDetailStartDate').setAttribute('value', moment(event.startDate, "DD-MM-yyy HH:mm").format('YYYY-MM-DDTHH:mm:ss'));
        eventDetailPageTemplateCopy.querySelector('#eventDetailEndDate').setAttribute('value', moment(event.endDate, "DD-MM-yyy HH:mm").format('YYYY-MM-DDTHH:mm:ss'));

        // duration select
        let durationSelect = eventDetailPageTemplateCopy.querySelector('#eventDetailSelectedDuration');
        this.createEventDurationSelect(durationSelect);
        durationSelect.value = this.calculateEventDuration(event.startDate, event.endDate);

        // append event listener
        eventDetailPageTemplateCopy.querySelector('#eventDetailSaveButton').addEventListener('click', this.controller.editCalendarEvent.bind(this.controller));
        eventDetailPageTemplateCopy.querySelector('#eventDetailDeleteButton').addEventListener('click', this.controller.deleteCalendarEvent.bind(this.controller));

        document.getElementById('eventDetailPageContent').appendChild(eventDetailPageTemplateCopy);
    }

    /******************************
     Calendar Event View Page
     *****************************/

    async createEventViewPage(eventViewCalendarId) {
        if (eventViewCalendarId === undefined || eventViewCalendarId === "") {
            window.location.href = "#/calendar";
        }
        let date = moment().format("DD-MM-yyy HH:mm");

        //create the next 30 days
        for (let i = 0; i < 7; i++) {
            await this.createEventViewPageDay(eventViewCalendarId, date);
            date = moment(date, "DD-MM-yyy HH:mm").add(1, 'days').format("DD-MM-yyy HH:mm");
        }
    }

    async createEventViewPageDay(eventViewCalendarId, date) {
        // get template
        let eventViewPageTemplate = document.getElementById('eventViewDayTemplate').content;
        let eventViewPageTemplateCopy = document.importNode(eventViewPageTemplate, true);
        // set values

        let shortDayLetter = moment(date, "DD-MM-yyy HH:mm").format('dddd').substr(0, 2);
        let shortNumber = moment(date, "DD-MM-yyy HH:mm").format('ll');
        shortNumber = shortNumber.split(",")[0];

        eventViewPageTemplateCopy.querySelector('.event-day-name').textContent = shortDayLetter;
        eventViewPageTemplateCopy.querySelector('.event-day-day').textContent = shortNumber;
        eventViewPageTemplateCopy.querySelector('.events-container').setAttribute("data-date", date);

        document.getElementById('calendarContent').appendChild(eventViewPageTemplateCopy);

        let events = await this.controller.getEventsFromCalendar(eventViewCalendarId, date);

        events.forEach(event => {
            this.createEventViewEvent(event, date);
        });
    }

    createEventViewEvent(event, date) {
        //create copy of template
        let eventTemplate = document.getElementById('event-template').content;
        let eventTemplateCopy = document.importNode(eventTemplate, true);

        eventTemplateCopy = this.createEventTemplate(event, eventTemplateCopy);

        document.querySelector("[data-date='" + date + "']").appendChild(eventTemplateCopy);
    }

}
