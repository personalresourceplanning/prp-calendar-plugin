/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

let translationLoader = new TranslationLoader();
translationLoader = translationLoader.loadTranslation();
let view = new CalendarView(translationLoader);

switch (window.location.href.split('#')[1].split('?')[0]) {
    case "/calendar":
        view.createCalendarTitle();
        view.createCalendarPlusButton();
        view.createCalendarItems();
        break;
    case "/calendar/dailyview":
        view.createCalendarTitle();
        view.createEventPlusButton();
        let dailyViewCalendarId, calendarDate;
        try {
            // get URL params
            let urlParams = window.location.href.split('#')[1].split('?')[1].split('&');
            // get the calender id of the url param
            dailyViewCalendarId = urlParams[0].substr(11);
            // get the date of the URL param
            calendarDate = decodeURI(urlParams[1].substr(5));
        } catch (e) {
        }
        view.createSwitchButton(dailyViewCalendarId);
        view.createDailyView(dailyViewCalendarId, calendarDate);
        break;
    case "/calendar/event":
        let eventId, eventDate;
        try {
            // get URL params
            let urlParams = window.location.href.split('#')[1].split('?')[1].split('&');
            // get the event id of the url param
            eventId = urlParams[0].substr(8);
            // get the date of the URL param
            eventDate = decodeURI(urlParams[1].substr(5));
        } catch (e) {
        }
        view.createEventDetailPage(eventId, eventDate);
        break;
    case "/calendar/eventview":
        view.createCalendarTitle();
        view.createEventPlusButton();
        let eventViewCalendarId;
        try {
            let urlParams = window.location.href.split('#')[1].split('?')[1].split('&');
            eventViewCalendarId = urlParams[0].substr(11);
        } catch (e) {
        }
        view.createSwitchButton(eventViewCalendarId);
        view.createEventViewPage(eventViewCalendarId);
        break;
    default:
        break;
}

function toggleAddMenu() {
    document.getElementById('addMenu').classList.toggle('add-menu-active');
    document.getElementById('overlay').classList.toggle('overlay-visible');
}

function removeAddMenu() {
    document.getElementById('addMenu').classList.remove('add-menu-active');
    document.getElementById('overlay').classList.remove('overlay-visible');
}
