/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

PLUGINMANAGER.register("calendarPlugin", function () {

    this.name = "Calendar";
    this.iconClass = "far fa-calendar-alt";
    this.homeUrl = "/calendar";
    this.ranking = 80;
    this.filePath = "calendar.html";

    this.subRoutes = [
        {
            name: 'dailyCalendarView',
            subPath: this.homeUrl + '/dailyview',
            filePath: 'dailyview.html',
        },
        {
            name: 'eventViewPage',
            subPath: this.homeUrl + '/eventview',
            filePath: 'eventView.html',
        },
        {
            name: 'eventDetailPage',
            subPath: this.homeUrl + '/event',
            filePath: 'eventDetailPage.html',
        }
    ];

});
