/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

class de {

    static DE() {
        const logger = new Logger("DE Tanslation");
        logger.log("Loading GERMAN for GERMANY.");
        return {
            "commons": {
                "save": "Speichern",
                "delete": "Löschen"
            },
            "calendar": {
                "titleCalendarPlugin": "Kalender",
                "title": "Titel",
                "startDate": "Startdatum",
                "endDate": "Enddatum",
                "duration": "Dauer",
                "description": "Beschreibung",
                "allDay": "Ganztägig",
                "location": "Ort",
                "errorTextEventEndDate": "Das Startdatum muss vor dem Enddatum liegen!",
                "errorTextCalendarTitle": "Bitte einen Titel eingeben!",
                "noTitle": "[kein Titel]",
                "addEvent": "Termin erstellen",
                "addCalendar": "Kalender erstellen",
                "calendarView": "Kalendaransicht",
                "eventView": "Eventansicht",
                "noCalendarFound": "Keine Kalender gefunden"
            }
        }
    }

}
